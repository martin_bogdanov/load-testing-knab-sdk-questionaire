'use strict';

module.exports = class QuestionRequests {
    constructor(baseHelpers) {
        this.baseHelpers = baseHelpers;
    }

    getQuestions(libraryId, captureResponse) {
        this.baseHelpers.setLog("Get Questions");

        let request;
        if (captureResponse) {
            request = {
                "get": {
                    "url": "/questions?libraryId={{ " + libraryId + " }}",
                    "capture": captureResponse
                }
            };
        } else {
            request = {
                "get": {
                    "url": "/questions?libraryId={{ " + libraryId + " }}"
                }
            };
        }

        this.baseHelpers.flow.push(request);
    }

    getQuestion(id) {
        this.baseHelpers.setLog("Get a library");

        this.baseHelpers.flow.push({
            "get": {
                "url": "/questions/{{ " + id + " }}"
            }
        });
    }

    getQuestionTypes(captureResponse) {
        this.baseHelpers.setLog('Get question types');

        let request;
        if (captureResponse) {
            request = {
                "get": {
                    "url": "/questionTypes",
                    "capture": captureResponse
                }
            };
        } else {
            request = {
                "get": {
                    "url": "/questionTypes"
                }
            };
        }

        this.baseHelpers.flow.push(request);
    }

    addQuestion(name, libraryId, questionTypeId, captureResponse) {
        this.baseHelpers.setLog("Add a question");

        let request;
        if (captureResponse) {
            request = {
                "post": {
                    "url": "/questions",
                    "json": {
                        "name": name,
                        "libraryId": "{{ " + libraryId + " }}",
                        "questionTypeId": "{{ " + questionTypeId + " }}"
                    },
                    "capture": captureResponse
                }
            }
        } else {
            request = {
                "post": {
                    "url": "/questions",
                    "json": {
                        "name": name,
                        "libraryId": "{{ " + libraryId + " }}",
                        "questionTypeId": "{{ " + questionTypeId + " }}"
                    }
                }
            }
        }
        this.baseHelpers.flow.push(request);
    }

    editQuestion(id) {
        this.baseHelpers.setLog("Edit a question");

        let request = {
            "put": {
                "url": "/questions",
                "json": {
                    "id": "{{ " + id + " }}"
                }
            }
        };
        this.baseHelpers.flow.push(request);
    }

    deleteQuestion(id) {
        this.baseHelpers.setLog("Delete a question");

        let request = {
            "delete": {
                "url": "/questions",
                "json": {
                    "id": "{{ " + id + " }}"
                }
            }
        };
        this.baseHelpers.flow.push(request);
    }
}