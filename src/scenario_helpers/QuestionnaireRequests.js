'use strict';

module.exports = class QuestionnaireRequests {
    constructor(baseHelpers) {
        this.baseHelpers = baseHelpers;
    }

    getQuestionnaires(libraryId, captureResponse) {
        this.baseHelpers.setLog("Get Questionnaires");

        let request;
        if (captureResponse) {
            request = {
                "get": {
                    "url": "/questionnaires?libraryId={{ " + libraryId + " }}",
                    "capture": captureResponse
                }
            };
        } else {
            request = {
                "get": {
                    "url": "/questionnaires?libraryId={{ " + libraryId + " }}",
                }
            };
        }

        this.baseHelpers.flow.push(request);
    }

    getQuestionnaire(id) {
        this.baseHelpers.setLog("Get a questionnaire");

        this.baseHelpers.flow.push({
            "get": {
                "url": "/questionnaires/{{ " + id + " }}"
            }
        });
    }

    getIdentifications(captureResponse) {
        this.baseHelpers.setLog('Get identifications');

        let request;
        if (captureResponse) {
            request = {
                "get": {
                    "url": "/identifications",
                    "capture": captureResponse
                }
            };
        } else {
            request = {
                "get": {
                    "url": "/identifications"
                }
            };
        }

        this.baseHelpers.flow.push(request);
    }

    addQuestionnaire(name, libraryId, identificationId, captureResponse) {
        this.baseHelpers.setLog("Add a questionnaire");

        let request;
        if (captureResponse) {
            request = {
                "post": {
                    "url": "/questionnaires",
                    "json": {
                        "name": name,
                        "libraryId": "{{ " + libraryId + " }}",
                        "identificationId": "{{ " + identificationId + " }}"
                    },
                    "capture": captureResponse
                }
            }
        } else {
            request = {
                "post": {
                    "url": "/questionnaires",
                    "json": {
                        "name": name,
                        "libraryId": "{{ " + libraryId + " }}",
                        "identificationId": "{{ " + identificationId + " }}"
                    }
                }
            }
        }
        this.baseHelpers.flow.push(request);
    }

    addQuestionnaireWithQuestions(name, libraryId, identificationId, questionIds, captureResponse) {
        this.baseHelpers.setLog("Add a questionnaire with questions");

        let request = {
            "post": {
                "url": "/questionnaires",
                "json": {
                    "name": name,
                    "libraryId": "{{ " + libraryId + " }}",
                    "identificationId": "{{ " + identificationId + " }}",
                    "questionIds": questionIds
                },
                "capture": captureResponse
            }
        };
        this.baseHelpers.flow.push(request);
    }

    addQuestionnaireWithQuestionnaireAndQuestions(name, libraryId, identificationId, questionIds, questionnaireIds, captureResponse) {
        this.baseHelpers.setLog("Add a questionnaire with questions and a questeionnaire");

        let request = {
            "post": {
                "url": "/questionnaires",
                "json": {
                    "name": name,
                    "libraryId": "{{ " + libraryId + " }}",
                    "identificationId": "{{ " + identificationId + " }}",
                    "questionIds": questionIds,
                    "questionnaireIds": questionnaireIds
                },
                "capture": captureResponse
            }
        };
        this.baseHelpers.flow.push(request);
    }

    editQuestionnaireWithQuestionnaireAndQuestions(name, libraryId, identificationId, questionnaireIds, captureResponse) {
        this.baseHelpers.setLog("Edit a questionnaire by adding a questionnaire with questions");

        let request = {
            "put": {
                "url": "/questionnaires",
                "json": {
                    "name": name,
                    "libraryId": "{{ " + libraryId + " }}",
                    "identificationId": "{{ " + identificationId + " }}",
                    "questionnaireIds": questionnaireIds
                },
                "capture": captureResponse
            }
        };
        this.baseHelpers.flow.push(request);
    }

    editQuestionnaireWithQuestions(name, libraryId, identificationId, questionIds, captureResponse) {
        this.baseHelpers.setLog("Edit a questionnaire by adding questions");

        let request = {
            "put": {
                "url": "/questionnaires",
                "json": {
                    "name": name,
                    "libraryId": "{{ " + libraryId + " }}",
                    "identificationId": "{{ " + identificationId + " }}",
                    "questionIds": questionIds
                },
                "capture": captureResponse
            }
        };
        this.baseHelpers.flow.push(request);
    }

    deleteQuestionnaire(id) {
        this.baseHelpers.setLog("Delete a questionnaire");

        let request = {
            "delete": {
                "url": "/questionnaires",
                "json": {
                    "id": "{{ " + id + " }}"
                }
            }
        };
        this.baseHelpers.flow.push(request);
    }
}