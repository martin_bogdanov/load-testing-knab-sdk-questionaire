'use strict';

let fs = require('fs');
let npmRun = require('npm-run');
let RootHelpers = require('./RootHelpers');
let rootHelpers = new RootHelpers();
let LibraryRequests = require('./LibraryRequests');
let libraryRequests = new LibraryRequests(rootHelpers);
let QuestionRequests = require('./QuestionRequests');
let questionRequests = new QuestionRequests(rootHelpers);
let QuestionnaireRequests = require('./QuestionnaireRequests');
let questionnaireRequests = new QuestionnaireRequests(rootHelpers);

module.exports = {
    root: rootHelpers,
    libraryRequests: libraryRequests,
    questionRequests: questionRequests,
    questionnaireRequests: questionnaireRequests,
    saveScenarioToJsonFile,
    runLoadTest
};

function saveScenarioToJsonFile(filename) {
    let filePath = `tests/generated_json/${filename}.json`;
    let json = JSON.stringify(rootHelpers.test, null, 4);
    fs.writeFileSync(filePath, json, 'utf8');
    return filePath;
}

function runLoadTest(filePath) {
    return new Promise(function (resolve, reject) {
        npmRun.exec('artillery run -e local ' + filePath, {maxBuffer: 3000 * 1024}, function (err, stdout, stderr) {
            if (err) {
                reject('ERR: ' + err);
            }           
            resolve(`${stderr} \n ${stdout}`);
        });        
    });
}