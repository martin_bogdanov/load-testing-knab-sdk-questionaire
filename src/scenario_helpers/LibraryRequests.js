'use strict';

module.exports = class LibraryRequests {
    constructor(baseHelpers) {
        this.baseHelpers = baseHelpers;
    }

    getLibraries(captureResponse) {
        this.baseHelpers.setLog("Get Libraries");

        let request;
        if (captureResponse) {
            request = {
                "get": {
                    "url": "/libraries",
                    "capture": captureResponse
                }
            };
        } else {
            request = {
                "get": {
                    "url": "/libraries"
                }
            };
        }

        this.baseHelpers.flow.push(request);
    }

    getLibrary(id) {
        this.baseHelpers.setLog("Get a library");

        this.baseHelpers.flow.push({
            "get": {
                "url": "/libraries/{{ " + id + " }}"
            }
        });
    }

    addLibrary(name, captureResponse) {
        this.baseHelpers.setLog("Add a library");

        let request = {
            "post": {
                "url": "/libraries",
                "json": {
                    "name": name
                },
                "capture": captureResponse
            }
        };
        this.baseHelpers.flow.push(request);
    }

    editLibrary(id) {
        this.baseHelpers.setLog("Edit a library");

        let request = {
            "put": {
                "url": "/libraries",
                "json": {
                    "id": "{{ " + id + " }}"
                }
            }
        };
        this.baseHelpers.flow.push(request);
    }

    deleteLibrary(id) {
        this.baseHelpers.setLog("Delete a library");

        let request = {
            "delete": {
                "url": "/libraries",
                "json": {
                    "id": "{{ " + id + " }}"
                }
            }
        };
        this.baseHelpers.flow.push(request);
    }
}