'use strict';

module.exports = class RootHelpers {
    constructor() {
        this.test = {};
        this.flow = [];        
    }

    setConfig(phases, httpPool) {            
        let config = this.test.config = {};
        let environments = config.environments = {};
        let local = environments.local = {};
        local.target = "http://localhost:4000";
        local.phases = phases;
        local.defaults = {};
        local.defaults.headers = {};
        local.defaults.headers["Content-Type"] = "application/json";
        local.processor = "../../src/artillery_helpers/LoadTestRunnerHelpers.js";
        local.http = {};
        local.http.timeout = 30;
        if (httpPool) {
            local.http.pool = httpPool;
        }            
    }

    initScenarioAndFlow() {
        this.test.scenarios = [];
        this.test.scenarios.push({});
    }

    setLog(text) {
        this.flow.push({ "log": text });
    }

    endFlow() {
        this.test.scenarios[0].flow = this.flow;
    }
} 