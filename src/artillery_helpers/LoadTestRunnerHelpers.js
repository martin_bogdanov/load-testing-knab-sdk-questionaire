"use strict";

module.exports = {
    convertIdsToInt: convertIdsToInt,
    logRequest: logRequest,
    logResponse: logResponse
}

function convertIdsToInt(requestParams, context, ee, next) {        
    convertIdToInt("id", requestParams, context);
    convertIdToInt("libraryId", requestParams, context);
    convertIdToInt("questionTypeId", requestParams, context);    
    convertIdToInt("identificationId", requestParams, context);
    return next(); // MUST be called for the scenario to continue
}

function logRequest(requestParams, context, ee, next) {
    console.log("====================");
    console.log("request params: " + JSON.stringify(requestParams));
    return next(); // MUST be called for the scenario to continue
}

function logResponse(requestParams, response, context, ee, next) {
    console.log("====================");
    console.log("response body: " + JSON.stringify(response.body));
    return next(); // MUST be called for the scenario to continue
}

function convertIdToInt(name, requestParams, context) {
    let value = requestParams.json[name];
    if (value) {
        value = context.vars[removeValuePreservedSymbols(value)];
        requestParams.json[name] = value;
    } 
}

function removeValuePreservedSymbols(capturedVar) {
    return capturedVar
        .replace(/{{/g, "")
        .replace(/}}/g, "")
        .trim();
}