let scenario = require('../src/scenario_helpers/scenarioGenerator');

describe('Questionnaires load tests', function () {    
    it('CRUD questionnaire', function () {
        let phases = [
            {
                "duration": 30,
                "arrivalRate": 5
            }
        ];
        scenario.root.setConfig(phases);
        scenario.root.initScenarioAndFlow();

        let captureResponse = { "json": "$.libraries[0].id", "as": "libraryId1" };
        scenario.libraryRequests.getLibraries(captureResponse);

        captureResponse = { "json": "$.questionnaires[0].id", "as": "questionnaireId1" };
        scenario.questionnaireRequests.getQuestionnaires("libraryId1", captureResponse);

        scenario.questionnaireRequests.getQuestionnaire("questionnaireId1");

        captureResponse = { "json": "$.identifications[0].id", "as": "identificationId1" };
        scenario.questionnaireRequests.getIdentifications(captureResponse);

        captureResponse = { "json": "id", "as": "questionnaireId1" };
        scenario.questionnaireRequests.addQuestionnaire("New Questionnaire", "libraryId1", "identificationId1", captureResponse);        

        scenario.questionnaireRequests.deleteQuestionnaire("questionnaireId1");

        scenario.root.endFlow();
        const filePath = scenario.saveScenarioToJsonFile("QuestionnairesLoadTest");

        return scenario.runLoadTest(filePath)
            .then(function (output) {
                console.log(output);
            }, function (err) {
                throw err;
            });
    });
});