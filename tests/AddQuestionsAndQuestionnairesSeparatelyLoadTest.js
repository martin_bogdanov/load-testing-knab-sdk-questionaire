let scenario = require('../src/scenario_helpers/scenarioGenerator');

describe('Full scenario 2', function () {
    it('should add both questions and questionnaires of a library without coupling', function () {
        let phases = [
            {
                "duration": 20,
                "arrivalRate": 5
            }
        ];
        scenario.root.setConfig(phases);
        scenario.root.initScenarioAndFlow();

        let captureResponse = { "json": "id", "as": "newLibraryId" };
        scenario.libraryRequests.addLibrary("New library", captureResponse);

        captureResponse = { "json": "$.questionTypes[0].id", "as": "questionTypeId1" };
        scenario.questionRequests.getQuestionTypes(captureResponse);

        for (let i = 0; i < 3; i++) {
            scenario.questionRequests.addQuestion("New Question" + (i + 1), "libraryId1", "questionTypeId1");
        }

        captureResponse = { "json": "$.identifications[0].id", "as": "identificationId1" };
        scenario.questionnaireRequests.getIdentifications(captureResponse);

        for (let i = 0; i < 3; i++) {
            scenario.questionnaireRequests.addQuestionnaire("New Questionnaire", "libraryId1", "identificationId1");
        }

        scenario.root.endFlow();
        const filePath = scenario.saveScenarioToJsonFile("AddQuestionsAndQuestionnairesSeparatelyLoadTest");

        return scenario.runLoadTest(filePath)
            .then(function (output) {
                console.log(output);
            }, function (err) {
                throw err;
            });
    });
});