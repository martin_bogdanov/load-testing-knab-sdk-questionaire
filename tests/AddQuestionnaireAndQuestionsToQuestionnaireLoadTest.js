let scenario = require('../src/scenario_helpers/scenarioGenerator');

describe('Full scenario 1', function () {        
    it('should add a questionnaire with both questionnaire and questions', function () {
        let phases = [
            {
                "duration": 5,
                "arrivalRate": 2
            },
            {
                "duration": 10,
                "arrivalRate": 2,
                "rampUp": 5
            },
            {
                "duration": 15,
                "arrivalRate": 10
            }
        ];
        scenario.root.setConfig(phases);
        scenario.root.initScenarioAndFlow();

        let captureResponse = { "json": "id", "as": "newLibraryId" };
        scenario.libraryRequests.addLibrary("New library", captureResponse);

        captureResponse = { "json": "$.questionTypes[0].id", "as": "questionTypeId1" };
        scenario.questionRequests.getQuestionTypes(captureResponse);

        captureResponse = { "json": "id", "as": "questionId1" };
        scenario.questionRequests.addQuestion("New Question", "libraryId1", "questionTypeId1", captureResponse);

        captureResponse = { "json": "id", "as": "questionId2" };
        scenario.questionRequests.addQuestion("New Question", "libraryId1", "questionTypeId1", captureResponse);

        captureResponse = { "json": "$.identifications[0].id", "as": "identificationId1" };
        scenario.questionnaireRequests.getIdentifications(captureResponse);

        let questionIds = ["{{ questionId1 }}", "{{ questionId2 }}"];
        captureResponse = { "json": "id", "as": "questionnaireId1" };
        scenario.questionnaireRequests.addQuestionnaireWithQuestions(
            "New Questionnaire", "libraryId1", "identificationId1", questionIds, captureResponse);

        let questionnaireIds = ["{{ questionnaireId1 }}"];
        scenario.questionnaireRequests.addQuestionnaireWithQuestionnaireAndQuestions(
            "New Questionnaire", "libraryId1", "identificationId1", questionIds, questionnaireIds, captureResponse);

        scenario.root.endFlow();
        const filePath = scenario.saveScenarioToJsonFile("AddQuestionnaireAndQuestionsToQuestionnaireLoadTest");

        return scenario.runLoadTest(filePath)
            .then(function (output) {
                console.log(output);
            }, function (err) {
                throw err;
            });
    });
});