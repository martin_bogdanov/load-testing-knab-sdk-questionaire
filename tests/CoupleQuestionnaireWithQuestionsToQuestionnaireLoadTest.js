let scenario = require('../src/scenario_helpers/scenarioGenerator');

describe('Full scenario 3', function () {    
    it('should couple a questionnaire with questions to a questionnaire', function () {
        let phases = [
            {
                "duration": 60,
                "arrivalCount": 50
            }
        ];
        scenario.root.setConfig(phases);
        scenario.root.initScenarioAndFlow();

        let captureResponse = { "json": "id", "as": "newLibraryId" };
        scenario.libraryRequests.addLibrary("New library", captureResponse);

        captureResponse = { "json": "$.questionTypes[0].id", "as": "questionTypeId1" };
        scenario.questionRequests.getQuestionTypes(captureResponse);

        captureResponse = { "json": "id", "as": "questionId1" };
        scenario.questionRequests.addQuestion("New Question", "libraryId1", "questionTypeId1", captureResponse);

        captureResponse = { "json": "id", "as": "questionId2" };
        scenario.questionRequests.addQuestion("New Question", "libraryId1", "questionTypeId1", captureResponse);

        captureResponse = { "json": "$.identifications[0].id", "as": "identificationId1" };
        scenario.questionnaireRequests.getIdentifications(captureResponse);

        let questionIds = ["{{ questionId1 }}", "{{ questionId2 }}"];
        captureResponse = { "json": "id", "as": "questionnaireId1" };
        scenario.questionnaireRequests.addQuestionnaireWithQuestions(
            "New Questionnaire", "libraryId1", "identificationId1", questionIds, captureResponse);

        let questionnaireIds = ["{{ questionnaireId1 }}"];
        scenario.questionnaireRequests.editQuestionnaireWithQuestionnaireAndQuestions(
            "New Questionnaire", "libraryId1", "identificationId1", questionnaireIds, captureResponse);

        scenario.root.endFlow();
        const filePath = scenario.saveScenarioToJsonFile("CoupleQuestionnaireWithQuestionsToQuestionnaireLoadTest");

        return scenario.runLoadTest(filePath)
            .then(function (output) {
                console.log(output);
            }, function (err) {
                throw err;
            });
    });
});