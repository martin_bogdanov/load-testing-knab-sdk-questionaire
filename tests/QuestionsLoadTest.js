let scenario = require('../src/scenario_helpers/scenarioGenerator');

describe('Questions load tests', function () {    
    it('CRUD question', function () {
        let phases = [
            {
                "duration": 30,
                "arrivalRate": 5
            }
        ];
        scenario.root.setConfig(phases);
        scenario.root.initScenarioAndFlow();

        let captureResponse = { "json": "$.libraries[0].id", "as": "libraryId1" };
        scenario.libraryRequests.getLibraries(captureResponse);

        captureResponse = { "json": "$.questions[0].id", "as": "questionId1" };
        scenario.questionRequests.getQuestions("libraryId1", captureResponse);

        scenario.questionRequests.getQuestion("questionId1");

        captureResponse = { "json": "$.questionTypes[0].id", "as": "questionTypeId1" };
        scenario.questionRequests.getQuestionTypes(captureResponse);

        captureResponse = { "json": "id", "as": "newQuestionId1" };
        scenario.questionRequests.addQuestion("New Question", "libraryId1", "questionTypeId1", captureResponse);

        scenario.questionRequests.editQuestion("newQuestionId1");

        scenario.questionRequests.deleteQuestion("newQuestionId1");

        scenario.root.endFlow();
        const filePath = scenario.saveScenarioToJsonFile("QuestionsLoadTest");

        return scenario.runLoadTest(filePath)
            .then(function (output) {
                console.log(output);
            }, function (err) {
                throw err;
            });
    });
});