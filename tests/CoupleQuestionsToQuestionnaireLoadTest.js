let scenario = require('../src/scenario_helpers/scenarioGenerator');

describe('Full scenario 4', function () {    
    it('should couple questions to a questionnaire', function () {
        let phases = [            
            {
                "duration": 5,
                "arrivalRate": 1
            },
            {
                "pause": 5
            },
            {
                "duration": 10,
                "arrivalRate": 30
            }
        ];
        let httpPool = 10;        
        scenario.root.setConfig(phases, httpPool);
        scenario.root.initScenarioAndFlow();

        let captureResponse = { "json": "id", "as": "newLibraryId" };
        scenario.libraryRequests.addLibrary("New library", captureResponse);

        captureResponse = { "json": "$.identifications[0].id", "as": "identificationId1" };
        scenario.questionnaireRequests.getIdentifications(captureResponse);

        captureResponse = { "json": "id", "as": "questionnaireId1" };
        scenario.questionnaireRequests.addQuestionnaire(
            "New Questionnaire", "libraryId1", "identificationId1", captureResponse);

        captureResponse = { "json": "id", "as": "questionId1" };
        scenario.questionRequests.addQuestion("New Question 1", "libraryId1", "questionTypeId1", captureResponse);

        captureResponse = { "json": "id", "as": "questionId2" };
        scenario.questionRequests.addQuestion("New Question 2", "libraryId1", "questionTypeId1", captureResponse);

        let questionIds = ["{{ questionId1 }}", "{{ questionId2 }}"];
        scenario.questionnaireRequests.editQuestionnaireWithQuestions(
            "New Questionnaire", "libraryId1", "identificationId1", questionIds, captureResponse);

        scenario.root.endFlow();
        const filePath = scenario.saveScenarioToJsonFile("CoupleQuestionsToQuestionnaireLoadTest");

        return scenario.runLoadTest(filePath)
            .then(function (output) {
                console.log(output);
            }, function (err) {
                throw err;
            });
    });
});