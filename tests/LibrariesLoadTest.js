let scenario = require('../src/scenario_helpers/scenarioGenerator');

describe('Libraries load tests', function () {    
    it('CRUD library', function () {
        let phases = [
            {
                "duration": 30,
                "arrivalRate": 5
            }
        ];
        scenario.root.setConfig(phases);
        scenario.root.initScenarioAndFlow();

        let captureResponse = { "json": "$.libraries[0].id", "as": "libraryId1" };
        scenario.libraryRequests.getLibraries(captureResponse);
        
        scenario.libraryRequests.getLibrary("libraryId1");

        captureResponse = { "json": "id", "as": "newLibraryId" };
        scenario.libraryRequests.addLibrary("New library", captureResponse);

        scenario.libraryRequests.editLibrary("newLibraryId");

        scenario.libraryRequests.deleteLibrary("newLibraryId");

        scenario.root.endFlow();
        const filePath = scenario.saveScenarioToJsonFile("LibrariesLoadTest");

        return scenario.runLoadTest(filePath)
            .then(function (output) {
                console.log(output);
            }, function (err) {
                throw err;
            });
    });
});