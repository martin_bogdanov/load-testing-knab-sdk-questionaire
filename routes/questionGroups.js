module.exports = function (app) {
    app.get('/questionnaires/:id', function (req, res) {
        res.send('Get a question group with id: ' + req.params.id);
    });

    app.route('/questionnaires')
        .get(function (req, res) {
            if (req.query.libraryId) {
                let response = {};
                response.questionnaires = [];            
                for(let i = 0; i < 3; i++) {
                    response.questionnaires.push({
                        "id": i + 1,
                        "name": "QuestionGroup " + (i + 1)
                    });
                }                        
                res.send(response);
                return;
            }
            res.status(422).send('Invalid query parameters!')
        })
        .post(function (req, res) {            
            let response = {
                "id": 1000,
                "libraryId": req.body.libraryId,
                "name": req.body.name,
                "questionIds": req.body.questionIds
            };
            res.send(response);
        })        
        .put(function (req, res) {
            let response = {                
            };
            res.send(response);
        })        
        .delete(function (req, res) {
            res.send('Delete a question group with id: ' + req.body.id);
        });
};