module.exports = function (app) {
    app.route('/questionTypes')
        .get(function (req, res) {
            let response = {};
            response.questionTypes = [];            
            for(let i = 0; i < 3; i++) {
                response.questionTypes.push({
                    "id": i + 1,
                    "name": "QuestionType " + (i + 1)
                });
            }                        
            res.send(response);
        })
        .post(function (req, res) {            
            let response = {
                "id": 1000,
                "name": ""
            };            
            res.send(response);
        })
        .put(function (req, res) {
            res.send('Edit identification with id: ' + req.body.id);
        })
        .delete(function (req, res) {
            res.send('Delete identification with id: ' + req.body.id);
        });    
};