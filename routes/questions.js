module.exports = function (app) {
    app.get('/questions/:id', function (req, res) {
        res.send('Get a question with id: ' + req.params.id);
    });

    app.route('/questions')
        .get(function (req, res) {
            if (req.query.libraryId) {
                let response = {};
                response.questions = [];            
                for(let i = 0; i < 3; i++) {
                    response.questions.push({
                        "id": i + 1,
                        "name": "Question " + (i + 1)
                    });
                }                        
                res.send(response);
                return;
            }
            res.status(422).send('Invalid query parameters!')
        })
        .post(function (req, res) {
            let response = {
                "id": 100,
                "libraryId": req.body.libraryId,
                "questionTypeId": req.body.questionTypeId,                
                "name": req.body.name,

            };            
            res.send(response);
        })
        .put(function (req, res) {
            res.send('Edit a question with id: ' + req.body.id);
        })
        .delete(function (req, res) {
            res.send('Delete a question with id: ' + req.body.id);
        });
};