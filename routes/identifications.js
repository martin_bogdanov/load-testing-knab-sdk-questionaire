module.exports = function (app) {
    app.route('/identifications')
        .get(function (req, res) {
            let response = {};
            response.identifications = [];            
            for(let i = 0; i < 3; i++) {
                response.identifications.push({
                    "id": i + 1                    
                });
            }                        
            res.send(response);
        })
        .post(function (req, res) {            
            let response = {
                id: 1000
            };            
            res.send(response);
        })
        .put(function (req, res) {
            res.send('Edit identification with id: ' + req.body.id);
        })
        .delete(function (req, res) {
            res.send('Delete identification with id: ' + req.body.id);
        });    
};