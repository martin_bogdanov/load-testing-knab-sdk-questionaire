module.exports = function (app) {
    app.get('/libraries/:id', function (req, res) {
        res.send('Get library with id: ' + req.params.id);
    });

    app.route('/libraries')
        .get(function (req, res) {
            let response = {};
            response.libraries = [];            
            for(let i = 0; i < 3; i++) {
                response.libraries.push({
                    "id": i + 1,
                    "name": "Library " + (i + 1)
                });
            }                        
            res.send(response);
        })
        .post(function (req, res) {            
            let response = {
                id: 10, 
                name: req.body.name
            };            
            res.send(response);
        })
        .put(function (req, res) {
            res.send('Edit library with id: ' + req.body.id);
        })
        .delete(function (req, res) {
            res.send('Delete library with id: ' + req.body.id);
        });    
};