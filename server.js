var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: false }));
//app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json({ type: 'application/*+json' }));

// app.use(function(req,res,next){
//     setTimeout(next,3000)
// });

require('./routes/libraries')(app);
require('./routes/questionTypes')(app);
require('./routes/questions')(app);
require('./routes/identifications')(app);
require('./routes/questionGroups')(app);

var server = app.listen(4000, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
});